from PIL import Image

class WinterAvata(Image.Image):
    # 自己的头像
    def __init__(self, image_name):
        self.avatar_image = Image.open(image_name);
        self.avatar_image.convert('RGBA')
        self.avatar_image_width_rate = 0.08
    # 模版层
    def createHappyAvatar(self, default='beijing'):
        image = Image.open('avatar_template/winter_{}.png'.format(default))
        image.convert('RGBA')
        image_width = int(image.size[0] * self.avatar_image_width_rate )
        re_size_w, re_size_h = image.size[0] - image_width, image.size[1] - image_width
        avatar = self.avatar_image.resize((re_size_w, re_size_h))
        temp_avatar = Image.new('RGBA', image.size)
        x1 = int(image_width / 2)
        y1 = int(image_width / 2)
        temp_avatar.paste(avatar, (x1, y1, x1 + re_size_w, y1 + re_size_h))
        temp_avatar.paste(image, (0, 0, 2184, 2184), image)
        temp_avatar.save('result/resultAvatar.png')